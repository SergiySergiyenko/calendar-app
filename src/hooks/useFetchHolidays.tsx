import React, { useEffect } from 'react';

interface State<T> {
  response?: T;
  error?: Error;
  isLoading?: boolean;
}

export function useFetch<T = unknown>(
  url: string,
  options?: RequestInit
): State<T> {
  const [response, setResponse] = React.useState<T>();
  const [error, setError] = React.useState<Error>();
  const [isLoading, setIsLoading] = React.useState(false);

  useEffect(() => {
    const fetchData = async () => {
      setIsLoading(true);
      try {
        const res = await fetch(url, options);
        const json = (await res.json()) as T;
        setResponse(json);
        setIsLoading(false);
      } catch (error) {
        setError(error as Error);
        setIsLoading(false);
      }
    };
    fetchData();
  }, [url, options]);

  return { response, error, isLoading };
}
