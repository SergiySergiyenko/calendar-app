import React, { FC, memo, useCallback } from 'react';
import { Droppable, Draggable } from 'react-beautiful-dnd';

import { Card } from './components/Card';
import { ShowModal, Task } from './types';
import { AddButton, LeftColumn, MonthDay, Row } from './task.style';

type Props = {
  tasks: Task[];
  listId: string;
  onToggle: () => void;
  setMonthDay: React.Dispatch<React.SetStateAction<string>>;
  setActiveTaskValues: React.Dispatch<React.SetStateAction<Task>>;
  listType?: string;
  internalScroll?: boolean;
  isCombineEnabled?: boolean;
};

export const TaskList: FC<Props> = memo(
  ({ listId, listType, tasks, onToggle, setMonthDay, setActiveTaskValues }) => {
    const showModal: ShowModal = useCallback(
      (task) => {
        onToggle();
        setMonthDay(listId);
        if (task) {
          setActiveTaskValues(task);
        }
      },
      [listId, setActiveTaskValues, setMonthDay]
    );

    return (
      <Droppable
        droppableId={listId}
        type={listType}
        direction='horizontal'
        isCombineEnabled={false}
      >
        {(dropProvided) => (
          <div {...dropProvided.droppableProps}>
            <div>
              <div>
                <Row ref={dropProvided.innerRef}>
                  <LeftColumn>
                    <MonthDay>{listId}</MonthDay>
                    <AddButton onClick={() => showModal()}>+</AddButton>
                  </LeftColumn>
                  {tasks.map((task, index) => (
                    <Draggable
                      key={task.id}
                      draggableId={task.id}
                      index={index}
                      isDragDisabled={task.fixed}
                    >
                      {(dragProvided) => (
                        <div
                          {...dragProvided.dragHandleProps}
                          {...dragProvided.draggableProps}
                          ref={dragProvided.innerRef}
                        >
                          <Card task={task} showModal={showModal} />
                        </div>
                      )}
                    </Draggable>
                  ))}
                  {dropProvided.placeholder}
                </Row>
              </div>
            </div>
          </div>
        )}
      </Droppable>
    );
  }
);
