import styled from 'styled-components';

export const Layout = styled.div`
  padding: 1rem;
`;

export const FilterPanel = styled.div`
  display: flex;
  align-items: baseline;
  gap: 1rem;
  margin: 1rem;
`;

export const ActionPanel = styled.div`
  display: flex;
  align-items: baseline;
  justify-content: space-evenly;
  margin: 1rem;
`;

export const Form = styled.form`
  min-width: 40vw;
  display: flex;
  flex-direction: column;
  gap: 0.4rem;
`;

export const Input = styled.input`
  width: 90%;
`;

export const TextArea = styled(Input)``;

export const Select = styled.select`
  width: 90%;
`;

export const FormFooter = styled.div`
  margin: 1rem;
  display: flex;
  justify-content: space-evenly;
`;
