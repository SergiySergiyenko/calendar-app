import styled from 'styled-components';

export const Row = styled.div`
  display: flex;
  border: 1px solid gray;
  width: 95vw;
  min-height: 130px;
  overflow-x: auto;
`;

export const LeftColumn = styled.div`
  width: 30px;
  display: flex;
  flex-direction: column;
  justify-content: space-around;
  align-items: center;
  border-right: 1px solid gray;
`;

export const MonthDay = styled.div`
  font-weight: bold;
  text-decoration: underline;
`;

export const AddButton = styled.button`
  padding-block: 4px;
  box-shadow: -1px 1px;
`;
