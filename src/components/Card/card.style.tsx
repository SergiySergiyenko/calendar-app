import styled from 'styled-components';

export const CardTitle = styled.div`
  width: 100px;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  margin: 0 auto;
  font-weight: 500;
`;

export const CardBody = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100px;
  height: 100px;
  margin: 5px;
  padding: 2px;
  background-color: ${(props) => props.color};
`;

export const CardDescription = styled.span`
  display: -webkit-box;
  -webkit-box-orient: vertical;
  -webkit-line-clamp: 2;
  overflow: hidden;
  text-overflow: ellipsis;
`;

export const ColorLabel = styled.span`
  height: 10px;
  width: 10px;
  margin: 0 2px;
  background-color: ${(props) => props.color};
  border-radius: 50%;
  display: inline-block;
`;

export const TagLabel = styled.div`
  width: 30px;
  font-size: 12px;
  font-weight: bold;
`;

export const EditButton = styled.button`
  width: 30px;
  padding: 0;
  align-self: end;
`;
