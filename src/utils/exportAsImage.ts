import html2canvas from 'html2canvas';

const downloadImage = (blob: string, fileName: string) => {
  const link = document.createElement('a');
  link.download = fileName;

  link.href = blob;

  document.body.appendChild(link);
  link.click();
  document.body.removeChild(link);

  link.remove();
};

export const exportAsImage = async (
  element: HTMLDivElement | null,
  imageFileName: string
) => {
  if (!element) return;
  const canvas = await html2canvas(element);
  const image = canvas.toDataURL('image/png', 1.0);
  downloadImage(image, imageFileName);
};
