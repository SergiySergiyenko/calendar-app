import { Holidays, Task, TaskMap } from '../types';

const createRangeMap = (start: number, end: number): Record<string, Task[]> => {
  return Array(end - start + 1)
    .fill(0)
    .reduce((acc, _, idx) => {
      const key = (start + idx).toString();
      acc[key] = [];
      return acc;
    }, {});
};

export const convertHolyToMap = (holidays: Holidays[]): TaskMap => {
  const currentDate = new Date().getDate();
  const rangeMap = createRangeMap(currentDate + 1, currentDate + 7);
  holidays.forEach((h) => {
    const monthDay = h.date.slice(-2);
    const holyTask: Task = {
      id: ((Math.random() + 1) * 10).toString(36),
      title: h.name,
      description: h.localName,
      fixed: true,
    };
    rangeMap[monthDay]?.push(holyTask);
  });

  return rangeMap;
};
